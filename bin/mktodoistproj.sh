#!/bin/bash

echo 
echo "This program is invoked with mktodoistprj <projectname>"
echo 
echo

if [[ -z ${@} ]]; then
    echo "no project name! bad boy"
    exit 12
fi

PROJECT_NAME="☢️ ${@}"
API_TOKEN=${TODOIST_API_TOKEN}

PRJ_PAYLOAD="{_name_: _${PROJECT_NAME}_}"
NEWPROJECT=$(echo ${PRJ_PAYLOAD} | tr "_" "\"" )

# Create project
PROJECT_ID="$(curl -s "https://api.todoist.com/rest/v1/projects" \
    --http1.1 \
    -X POST \
    --data-raw "${NEWPROJECT}" \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer ${API_TOKEN}" | jq .id)"

TITLE_PAYLOAD="{=content=: =*  **${PROJECT_NAME}** =, =description=: =This project will be considered complete when =, =project_id=: =${PROJECT_ID}=, =label_ids=: [ 2157029990 ], =due_string=: =in 6 months= }"
PROJECT_TITLE_AS_TASK=$(echo ${TITLE_PAYLOAD} | tr "=" "\"" )

curl -s "https://api.todoist.com/rest/v1/tasks" \
    --http1.1 \
    -X POST \
    --data-raw "${PROJECT_TITLE_AS_TASK}" \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer ${API_TOKEN}"
