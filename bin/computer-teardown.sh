#!/bin/bash
# This gives you a rundown of the things to do when tearing down a computer before formatting

echo "are there uncommitted changes in the dotfiles?"
echo "did you save your custom.zsh in the dotfiles?"
echo "did you save your ssh private key (not that you should but i mean, the aegee servers have the old one etc)"
echo "did you perform backup on cloud and on hard drive?"