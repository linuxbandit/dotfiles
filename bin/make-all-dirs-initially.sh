#!/bin/bash
# This script does setup of the folders. Lazy way when ansible is not thrown
# (anche perche devo imparare a lanciare solo un task e non l'intero playbook)

DOWNLOAD_DIRS=( _images _tools _jtm _aegee _telegram )
CLOUD_DIRS=( MEGA Gsuite OneDrive Sync google-personal onedrive-personal )

# Creation of ~/Downloads children
for dir in "${DOWNLOAD_DIRS[@]}"; do
    mkdir "${HOME}/Downloads/${dir}"
done

# Creation of ~/_Cloud and its children
mkdir "${HOME}/_Cloud/"
for dir in "${CLOUD_DIRS[@]}"; do
    mkdir "${HOME}/_Cloud/${dir}"
done

# Creation of single folders
mkdir "${HOME}/Desktop/desktop_shite"
mkdir "${HOME}/Documents/000_InTray"
mkdir "${HOME}/Documents/000_InTray/_nobackup"