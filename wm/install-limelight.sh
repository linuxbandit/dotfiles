#!/bin/bash

# The current wd is the one that is launching the script i.e.
# the one containing _install.sh!
cd wm/limelight
make
ln -s "${PWD}/bin/limelight" /usr/local/bin/limelight
