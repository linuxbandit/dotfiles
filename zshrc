## P10K
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

##

#TODO second answer on https://stackoverflow.com/questions/12382499/looking-for-altleftarrowkey-solution-in-zsh

## Fab's
filesLocation=~/.dotfiles/zsh

source $filesLocation/common.zshrc

if [[ $(uname) == "Linux" ]]; then
    LINUX=true
    source $filesLocation/zshrc.server
else
    LINUX=false
    source ~/.dotfiles/zsh/powerlevel10k.zsh
    source ~/.dotfiles/zsh/p10k/powerlevel10k.zsh-theme
fi

# if a custom file is present, load it (varies between computers)
#  loaded last so it can override variables/tokens
# IF there is a file with aliases/functions for a job,
#  it should be loaded from the custom.zshrc
if [[ -f $filesLocation/custom.zshrc ]]; then
  source $filesLocation/custom.zshrc
fi;

## End of Fab-managed stuff

# NOTE: the below import of pyenvrc is unused, the real import is
#   in dev.zshrc -- therefore there is no need to actually
#   simlink like ln -s $HOME/.dotfiles/.pyenvrc $HOME/.pyenv/.pyenvrc
#   and we keep it because yeah, ansible
# ACTUALLY I should delete it and not import
# BEGIN ANSIBLE MANAGED BLOCK: pyenv
#if [ -e "$HOME/.pyenv/.pyenvrc" ]; then
#  source $HOME/.pyenv/.pyenvrc
#fi
# END ANSIBLE MANAGED BLOCK: pyenv

# Created by `pipx` on 2024-02-11 12:22:34
export PATH="$PATH:/Users/grasshopper/.local/bin"
