Plugin 'bling/vim-airline'

let g:airline_powerline_fonts = 1
" if !exists('g:airline_symbols')
"   let g:airline_symbols = {}
" endif
" let g:airline_symbols.space = "\ua0"
let timeoutlen = 50
let g:airline_symbols.branch = '✈'

let g:airline_section_b = \ %{HasPaste()}%F%m%r%h\ %w
let g:airline_section_y = \ %{fugitive#statusline()}\ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\,%c%V(%P)