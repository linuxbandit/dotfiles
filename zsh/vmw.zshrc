#!/bin/zsh

function vlt(){ aws-vault exec adm-01 -- "${@}" }
function vltprd(){ aws-vault exec prd-00 -- "${@}" }

function tgrunt(){
    aws-vault exec adm-01 -- terragrunt "${@}"
}

function check_if_rancher_is_running(){
    if ! pgrep -f "Rancher Desktop" > /dev/null; then
      open /Applications/Rancher\ Desktop.app
      sleep 20
      sudo ln -sf $HOME/.rd/docker.sock /var/run/docker.sock
    fi
}

function create_vpn_all(){
    VSS_USER="${1}@vmware.com"
    VSS_ENV=$2

    check_if_rancher_is_running

    # Create certificate
    aws-vault exec ${VSS_ENV} -- /usr/local/bin/vssops cert create --environment-name ${VSS_ENV} --name ${VSS_USER}

    create_vpn_config_and_send $VSS_USER $VSS_ENV

}

function create_vpn_config_and_send(){ #MUST BE IN "@vmware.com" FORMAT
    VSS_USER="${1}"
    VSS_ENV=$2

    check_if_rancher_is_running

    set -e

    # Create configuration
    aws-vault exec ${VSS_ENV} -- /usr/local/bin/vssops cert create-vpn-config --environment-name ${VSS_ENV} --name ${VSS_USER} --destination /here

    # Send configuration via email
    aws-vault exec prd-00 -- /usr/local/bin/vssops cert email-vpn-config --environment-name ${VSS_ENV} --email ${VSS_USER} -d /here

    set +e

}

function create_initial_user_password(){
    reset_user_password ${1}
}

function batch_initial_passwords(){

    USERS=( )
    for usr in ${USERS[@]}; do

    reset_user_password $usr

    done

}

function batch_vpn_certs(){

    USERS=( )
    ENVS=( )

    for usr in ${USERS[@]}; do

        for envir in ${ENVS[@]}; do
            create_vpn_all $usr $envir
        done

    done
}

function reset_user_password(){
    VSS_USER="${1}@vmware.com"
    NEW_PASSWORD=` gpg --gen-random --armor 1 20 `

    check_if_rancher_is_running

    #set -e
    aws-vault exec usr-01 -- /usr/local/bin/vssops provision generate-password --password ${NEW_PASSWORD} --email ${VSS_USER} -e usr-01

    aws-vault exec prd-00 -- /usr/local/bin/vssops provision email-password --password ${NEW_PASSWORD} --email ${VSS_USER} -e usr-01
    #set +e
}

function reset_my_password(){
    VSS_USER="bellicanof@vmware.com"
    NEW_PASSWORD="$(gpg --gen-random --armor 1 20)"

    check_if_rancher_is_running

    #set -e
    aws-vault exec adm-01 -- /usr/local/bin/vssops provision generate-password --password ${NEW_PASSWORD} --email ${VSS_USER} -e usr-01
    echo $NEW_PASSWORD
    #aws-vault exec prd-00 -- /usr/local/bin/vssops provision email-password --password ${NEW_PASSWORD} --email ${VSS_USER} -e usr-01
    #set +e
}

function rotate_password_and_access_key(){
    MY_VSS_USER="bellicanof@vmware.com"
    NEW_PASSWORD=$(gpg --gen-random --armor 1 20)

    check_if_rancher_is_running

    aws-vault rotate adm-01
    sleep 5
    aws-vault exec adm-01 -- /usr/local/bin/vssops provision generate-password --password ${NEW_PASSWORD} --email ${MY_VSS_USER} -e adm-01
    op item edit 'torasampdoria' password=${NEW_PASSWORD}
}

# create_vpn_all rpetra cicd-01

# sleep 240
# create_initial_user_password kadrevm
# sleep 4

# create_vpn_all kadrevm dev-00
# sleep 4

# create_vpn_all kadrevm cicd-01
# sleep 4



export AWS_VAULT_BACKEND=pass
export VSSOPS_ALLOW_AWS_ENV_VARS=1

# START VSSOPS via docker
unset vssops vs vu

export VSSOPS_CONTAINER_IMAGE="982407596874.dkr.ecr.us-east-1.amazonaws.com/vssops"
export VSSOPS_WRAPPER_PATH="/usr/local/bin/vssops"

if [ ! -f "${VSSOPS_WRAPPER_PATH}" ] ; then
    cat <<EOF > ${VSSOPS_WRAPPER_PATH}
#!/bin/sh

# Run the vssops container and pass in the AWS creds via environment variables
docker run --rm --tty --interactive \
    -e VSSOPS_ALLOW_AWS_ENV_VARS=1 \
    -e AWS_ACCESS_KEY_ID \
    -e AWS_SESSION_TOKEN \
    -e AWS_SECRET_ACCESS_KEY \
    -e AWS_DEFAULT_REGION \
    -e VSS_ENVIRONMENT \
    -e AMBASSADOR_AUTH_TOKEN \
    -e AMBASSADOR_URL \
    -e PYTHONDONTWRITEBYTECODE=1 \
    -e SWAGGER_UI_ENABLED \
    -e VSSOPS_DEBUG \
    --publish 8000:8000 \
    --volume ${PWD}:/here \
    ${VSSOPS_CONTAINER_IMAGE} vssops \${@}
EOF

eval chmod +x ${VSSOPS_WRAPPER_PATH}
fi

#vssops update
vu() {
    aws-vault exec ${VSS_ENVIRONMENT:-'dev-00'} -- docker pull ${VSSOPS_CONTAINER_IMAGE}
}

# END VSSOPS

