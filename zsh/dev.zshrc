#!/bin/zsh

export ANSIBLE_NOCOWS=1

## NODE
export NVM_DIR="$HOME/.nvm"
# This loads nvm
[ -s "/usr/local/opt/nvm/nvm.sh" ] && \. "/usr/local/opt/nvm/nvm.sh"
# This loads nvm bash_completion
#[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/usr/local/opt/nvm/etc/bash_completion.d/nvm"

FABS_FAVE_PYTHON_VERSION=3.10.3

#TODO FOR PYTHON: during install (via ansible?) create venv folder in documents
#     venv folder in docs: i think i lose the 'automatic activation of venv'
#     since i don't enter a folder with venv anymore
#     additionally, the environment is not portable anymore (in icloud).
#     lastly i kinda decided that i will always use poetry, which has a neat
#     way to do the venv automatically

# Initialise pyenv and pyenv-virtualenv if installed
if command -v pyenv 1>/dev/null 2>&1; then eval "$(pyenv init -)"; fi
if command -v pyenv-virtualenv-init 1>/dev/null 2>&1; then
    eval "$(pyenv virtualenv-init -)"
elif [ -d "${PYENV_ROOT}/plugins/pyenv-virtualenv" ]; then
    eval "$(pyenv virtualenv-init -)"
fi

# Add pyenv into path if installed into default location

export PATH="$(pyenv root)/shims:$PATH"

# you want the below to FALSE before installing pipx
export PIP_REQUIRE_VIRTUALENV=true
# just to be safe
export PIPX_DEFAULT_PYTHON="${HOME}/.pyenv/versions/${FABS_FAVE_PYTHON_VERSION}/bin/python"

function set_python_to_fabs {
  #TODO make it run ansible instead of doing it by itself
  pyenv install $FABS_FAVE_PYTHON_VERSION
  pyenv global  $FABS_FAVE_PYTHON_VERSION
  echo_succ("E BRAVO")
}

# Disable prompt changing
#export PYENV_VIRTUALENV_DISABLE_PROMPT=1

## new kid in the block: now poetry takes care of it for me. just use `poetry shell` to activate
## the venv. and if i create it with pyenv-virtualenv (or similar), it picks that - or does nothing
## if it is already detecting that one is activated

# NOTES which are now obsolete because I will rule with poetry:
# three options for virtualenv outside of poetry
#
# 1. pyenv-virtualenv
#
# CREA
# pyenv virtualenv [version] my-cool-project
#
# LIST
# pyenv virtualenvs
#
# ATTIVA / DISATTIVA
# pyenv activate <name>
# pyenv deactivate
#
# CANCELLA
# pyenv uninstall my-cool-project
#
# 2. venv nativo (i.e. python -m venv [/path/to/]my-cool-project)
# 3. virtualenvwrapper
#
# don't bother, USE POETRY. And #1, if anything
