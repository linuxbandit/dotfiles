#!/bin/zsh

#don't autocorrect for sensitive commands
alias mv='nocorrect mv -i'
alias cp='nocorrect cp -i'
alias rm='nocorrect rm -i'

#completion patterns and modes
zstyle ':completion:*' auto-description 'specify: %d'
#dunno
zstyle ':completion:*' completer _expand _complete _correct _approximate
## complete as much u can ..
#zstyle ':completion:*' completer _complete _list _oldlist _expand _ignored _match _correct _approximate _prefix
## complete less
#zstyle ':completion:*' completer _expand _complete _list _ignored _approximate
## complete minimal
#zstyle ':completion:*' completer _complete _ignored

## case completion
## case-insensitive (uppercase from lowercase) completion
#zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
## case-insensitive (all) completion
#zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
## case-insensitive,partial-word and then substring completion
#zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
##all the above
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'


zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
## Separate matches into groups
zstyle ':completion:*:matches' group true

if [[ $(uname) == "Linux" ]]; then eval "$(dircolors -b)"; fi
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''

zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
zstyle ':completion:*:killall:*' command 'ps -u $USER -o cmd,%cpu,tty'

## Describe options in full
zstyle ':completion:*:options' description true
zstyle ':completion:*:options' auto-description '%d'

## Describe each match group.
#zstyle ':completion:*:descriptions' format "%B---- %d%b"
#zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:descriptions' format $'%{\e[0;31m%}%d%{\e[0m%}'

#Warnings on no match
#zstyle ':completion:*:warnings' format '%B%U---- no match for: %d%u%b'
zstyle ':completion:*:warnings' format $'\n%{\e[0;31m%}%BNo matches for: %d%b%{\e[0m%}'

#No clue
#zstyle ':completion:*:messages' format '%B%U---- %d%u%b'
zstyle ':completion:*:messages' format $'%{\e[0;31m%}%d%{\e[0m%}'

#Prompt during correction
zstyle ':completion:*:corrections' format $'%{\e[0;31m%}%d (errors: %e)%{\e[0m%}'
