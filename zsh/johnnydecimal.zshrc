#!/bin/zsh

#FROM "Amarandus/zsh-johnnydecimal"


if [[ -z "$JOHNNYDECIMAL_BASE" ]];
then
    echo "No Johnny.Decimal basedir given";
    echo "Using ~/johnny";

    mkdir ~/johnny || echo "dir existing";
    export JOHNNYDECIMAL_BASE=~/johnny;
fi;

JD_FILLER="_-"
#echo "JD sourced"

# realpath() { #BETTER USE COREUTILS https://stackoverflow.com/questions/3572030/bash-script-absolute-path-with-os-x
#   OURPWD=$PWD
#   cd "$(dirname "$1")"
#   LINK=$(readlink "$(basename "$1")")
#   while [ "$LINK" ]; do
#     cd "$(dirname "$LINK")"
#     LINK=$(readlink "$(basename "$1")")
#   done
#   REALPATH="$PWD/$(basename "$1")"
#   cd "$OURPWD"
#   echo "$REALPATH"
# }

_johnny_fetchpath()
{
    unset retval;
    local targetpath=("${JOHNNYDECIMAL_BASE}/${_j_area}${JD_FILLER}"*"/${_j_category}${JD_FILLER}"*"/${_j_category}.${_j_unique}${JD_FILLER}"*);
    retval=$(realpath ${targetpath});
}

_johnny_fetchcat()
{
    unset retvalcat;
    local targetpath=("${JOHNNYDECIMAL_BASE}/${_j_area}${JD_FILLER}"*"/${_j_category}${JD_FILLER}"*);
    retvalcat=$(realpath ${targetpath});
}

_johnny_splitdecimal()
{
    _j_category=${1:0:2}
    _j_unique=${1:3:2}
    _j_unique_next=$( printf %02d $(( $_j_unique + 01 )) )

    _j_area_lower=$(expr "(" ${_j_category} / 10 ")" "*" 10)
    _j_area_upper=$(($_j_area_lower + 9));
    _j_area="${(l:2::0:)_j_area_lower}-${(l:2::0:)_j_area_upper}";
}

jcd()
{
    if [[ ! $# -eq 1 ]];
    then
	echo "Usage:";
	echo "$ jcd CATEGORY.UNIQUE"
	return;
    fi;
    _johnny_splitdecimal "$1";
    _johnny_fetchpath;
    cd "${retval}";
    #pushd "${retval}"; #was this
}

jcp()
{
    if [[ $# -lt 2 ]];
    then
	echo "Usage:";
	echo "$ jcp CATEGORY.UNIQUE SRC"
	return;
    fi;
    _johnny_splitdecimal "$1";
    _johnny_fetchpath;
    cp --no-clobber --verbose --recursive "${@:2}" "$retval";
}

jmv()
{
    if [[ $# -lt 2 ]];
    then
	echo "Usage:";
	echo "$ jcp CATEGORY.UNIQUE SRC"
	return;
    fi;
    _johnny_splitdecimal "$1";
    _johnny_fetchpath;
    mv --no-clobber --verbose "${@:2}" "$retval";
}


jmkarea() #X0-X9
{
    if [[ ! $# -eq 2 ]];
    then
	echo "Usage:";
	echo "$ jmkarea CATEGORY DESC";
	return;
    fi;

    _johnny_splitdecimal "${1}.00";
    mkdir --verbose "${JOHNNYDECIMAL_BASE}/${_j_area}${JD_FILLER}${2}"
}


jmkcat() #X4
{
    if [[ ! $# -eq 2 ]];
    then
	echo "Usage:";
	echo "$ jmkcat CATEGORY DESC";
	return;
    fi;

    _johnny_splitdecimal "$1";
    local targetpath=("${JOHNNYDECIMAL_BASE}/${_j_area}${JD_FILLER}"*);
    mkdir --verbose "$targetpath/${_j_category}${JD_FILLER}${2}";
}

jmkuni() #X4.yy
{
    if [[ ! $# -eq 2 ]];
    then
	echo "Usage:";
	echo "$ jmkuni CATEGORY.UNIQUE DESC";
	return;
    fi;

    _johnny_splitdecimal "$1";
    local targetpath=("${JOHNNYDECIMAL_BASE}/${_j_area}${JD_FILLER}"*"/${_j_category}${JD_FILLER}"*);
    mkdir --verbose "$targetpath/${_j_category}.${_j_unique}${JD_FILLER}${2}"
}

jmknext() #Find next unique
{
    if [[ ! $# -eq 2 ]]; then
      echo "Usage:";
      echo "$ jmknext CATEGORY DESC";
      return;
    fi;

    #jwdcat $1 ls -1

    #TODO: a last item not folder/folder with bad name is breaking this.
    #      Hazel demon will have to take care of the removal and categorisation in catch-all
    local lastdir=$(jwdcat "$1.00" ls | tail -n 1)
    _johnny_splitdecimal ${lastdir:0:5}

    jmkuni "$1.$_j_unique_next" "$2"
    # jwdcat $1 ls -1
}


jwd()
{
    if [[ $# -lt 2 ]];
    then
	echo "Usage:";
	echo "$ jwd CATEGORY.UNIQUE CMD [ARG [ARG [ARG ...]]]"
	return;
    fi;
    _johnny_splitdecimal "$1";
    _johnny_fetchpath;
    pushd "${retval}" > /dev/null;
    echo "Directory: ${retval}";
    ${2} "${@:3}";
    local retval=$?;
    popd > /dev/null;
    return $retval;
}

jwdcat()
{
    if [[ $# -lt 2 ]]; then
      echo "Usage:";
      echo "$ jwdcat CATEGORY CMD [ARG [ARG [ARG ...]]]"
      return;
    fi;
    _johnny_splitdecimal "$1.00";
    _johnny_fetchcat;
    pushd "${retvalcat}" > /dev/null;
    echo "Directory: ${retvalcat}";
    ${2} "${@:3}";
    local retval=$?;
    popd > /dev/null;
    return $retval;
}

_johnny_completeID()
{
    local state line;
    typeset -A opt_args;

    _arguments -C \
	       '1: :->cats' \
	       '*: :->args';

    case "$state" in
	(cats)
	    for i in $JOHNNYDECIMAL_BASE/*/*;
	    do
		local jdidlist=();
		local jddesclist=();
		local category=$(basename "$i");
		for j in "$i"/*; do
		    local uniq=$(basename "$j");
		    jdidlist+=("${uniq:0:5}");
		    jddesclist+=("${uniq}");
		done;
		compadd -l -a -J "$category" -X "$category" -d jddesclist jdidlist;
	    done;
	    ;;
	*)
	    _files
	    ;;
    esac;
}


if ! whence -w compdef > /dev/null; then
  autoload -Uz compinit && compinit
  echo "compinit should've been already loaded. Please check"
fi

compdef _johnny_completeID jcd;
compdef _johnny_completeID jcp;
compdef _johnny_completeID jmv;
compdef _johnny_completeID jwd;
