#!/bin/zsh

##show red skull if root
## Shows little symbol '±' if you're currently at a git repo and '°' all other times

# mix between prompt_char 1 and 2
function prompt_char {
    if [[ $UID -eq 0 ]]; then
        echo "%{$fg[red]%}☣☠☢%{$reset_color%}";
    else
        git branch >/dev/null 2>/dev/null && echo '±' && return
        echo '☯';
    fi
}

function install_k8s {
    if [[ "${LINUX}" == "true" ]]; then
        echo "not yet implemented!!1";
    else
        brew install kubernetes-cli kubectx kube-ps1 stern helm
        brew tap derailed/k9s && brew install k9s
        echo ""
        echo "remember to open a new shell"
    fi
}

function what-ssh-key() {
    tail ~/.ssh/id*pub
}

function rsync-vanished() {
    src=${1}
    dest=${2}
    # Check if parameters are provided
    if [[ -z $src || -z $dest ]]; then
        echo "Error: source and destination are required."
        echo "Usage: rsync-mvalert <src> <dest>"
        return 1
    fi

    # Define a filename with date and time
    datetime=$(date "+%Y-%m-%d_%H-%M-%S")
    output_file="${datetime}-rsync_output.txt"
    vanished_file="${datetime}-vanished_files.txt"

    rsync -avvhP --exclude '.DS_Store' --remove-source-files "${src}" "${dest}" 2>&1 | tee "${output_file}"
    rsync_status=${PIPESTATUS[0]}  # Get the exit status of the rsync command

    message="$(hostname): rsync completed to ${dest}"
    emoji=':ok:'
    channel='general'

    echo

    if [[ ! $rsync_status -eq 0 ]]; then
      message="$(hostname): rsync FAILED to ${dest}"
      emoji=':boom:'
      channel='expected-maintenance'
      echo
      echo "rsync ERRORED"
      echo
    fi

    grep 'vanished' "${output_file}" > "${vanished_file}"
    if [[ -s "${vanished_file}" ]]; then
        echo "The following files vanished during rsync:"
        cat "${vanished_file}"
    else
        echo "No files vanished during this run."
        find "${src}" -type d -empty -delete
    fi

    ~grasshopper/.config/notify.py "${message}" "${emoji}" "${channel}"

}

find_hidden_files_that_fucked_up_rsync() {
    if [ -d "$1" ]; then
        find "$1" -type f \( ! -name ".DS_Store" \) -print
    else
        echo "Directory $1 does not exist."
    fi
}

delete_DS-Store_files_that_fucked_up_rsync() {
    if [ -d "$1" ]; then
        find "$1" -name ".DS_Store" -type f -delete
        echo ".DS_Store files deleted in $1"
    else
        echo "Directory $1 does not exist."
    fi
}

delete_empty_folder_that_fucked_up_rsync() {
    if [ -d "$1" ]; then
        find "$1" -type d -empty -delete
        echo "Empty directories deleted in $1"
    else
        echo "Directory $1 does not exist."
    fi
}

# export RUMENTA_FOLDER=/Volumes/??/cantina
# export MEDIA_FOLDER=/Volumes/??/cantina
# export PICS_FOLDER=/Volumes/??/cantina

function add-dnsmasq-domain() {

    local domain=$(echo $1 | tr -d '.')
    local ip=$2
    local conf_file="$(brew --prefix)/etc/dnsmasq.conf"
    local resolver_folder="/etc/resolver"

    # Check if parameters are provided
    if [[ -z $domain || -z $ip ]]; then
        echo "Error: Domain and IP parameters are required."
        echo "Usage: add-dnsmasq-domain <domain> <ip>"
        return 1
    fi

    # Check if the configuration file exists
    if [[ ! -f $conf_file ]]; then
        echo "dnsmasq configuration file not found at $conf_file."
        return 1
    fi

    # Append the new configuration to the dnsmasq.conf
    echo "address=/.$domain/$ip" | sudo tee -a $conf_file > /dev/null

    if [[ $? -eq 0 ]]; then
        echo "Domain $domain with IP $ip added to dnsmasq configuration."
        # Restart dnsmasq to apply changes
        sudo brew services restart dnsmasq
        echo "dnsmasq has been restarted to apply changes."
        echo "nameserver 127.0.0.1" | sudo tee -a "$resolver_folder/$domain" > /dev/null
    else
        echo "Failed to add domain to dnsmasq configuration."
        return 1
    fi
}

function check-dnsmasq-domain() {

    local domain=$1
    local conf_file="$(brew --prefix)/etc/dnsmasq.conf"
    local resolver_folder="/etc/resolver"


    # Check if the configuration file exists
    if [[ ! -f $conf_file ]]; then
        echo "dnsmasq configuration file not found at $conf_file."
        return 1
    fi

    # Check if parameters are provided
    if [[ -z $domain ]]; then
        tail $conf_file
    else
        grep $domain $conf_file
    fi
}

function remove-dnsmasq-domain() {
    local domain=$(echo $1 | tr -d '.')
    local conf_file="$(brew --prefix)/etc/dnsmasq.conf"
    local resolver_folder="/etc/resolver"

    # Check if domain parameter is provided
    if [[ -z $domain ]]; then
        echo "Error: Domain parameter is required."
        echo "Usage: remove-dnsmasq-domain <domain>"
        return 1
    fi

    # Check if the configuration file exists
    if [[ ! -f $conf_file ]]; then
        echo "dnsmasq configuration file not found at $conf_file."
        return 1
    fi

    # Remove the domain configuration from dnsmasq.conf
    sudo sed -i'' "/address=\/.$domain\//d" $conf_file

    if [[ $? -eq 0 ]]; then
        echo "Domain $domain removed from dnsmasq configuration."
        # Restart dnsmasq to apply changes
        sudo brew services restart dnsmasq
        echo "dnsmasq has been restarted to apply changes."

        # Remove resolver file if it exists
        if [[ -f "$resolver_folder/$domain" ]]; then
            sudo rm "$resolver_folder/$domain"
            echo "Resolver file for $domain removed."
        fi
    else
        echo "Failed to remove domain from dnsmasq configuration."
        return 1
    fi
}

function precommit-hooks-install() {

    # Installs pre-commit hooks
    pre-commit install

    # Installs commit-msg hooks
    pre-commit install --hook-type commit-msg
    pre-commit install --hook-type pre-push

    echo "checking now"
    bash -c "pre-commit run -a"
}

function cat_or_bat {
  if command -v bat &>/dev/null; then
    bat "$@"
  else
    cat "$@"
  fi
}

#make directory and change to it
function mkcd(){ test -d "${1}" || mkdir "${1}" && cd "${1}"; }

if [[ $LINUX != "true" ]]; then

    #https://www.reddit.com/r/macapps/comments/f3au13/what_are_some_of_the_lesser_known_apps_you_use/fhk0xuj/
    function pfd {
      osascript 2>/dev/null << EOF
          tell application "Finder"
            return POSIX path of (target of window 1 as alias)
          end tell
EOF
    }

    #renamed because of collision with cdf() function in fzf
    function cdfinder {
        cd "$(pfd)"
    }

    # Flushing cache of DNSs
    function dns-flush {
      sudo dscacheutil -flushcache && sudo killall -HUP mDNSResponder
    }

    # Encode/decode pasteboard on the fly
    function enc64pb {
        pbpaste | base64 | pbcopy
    }

    function dec64pb {
        pbpaste | base64 -D | pbcopy
    }

    # Upgrade the whole system
    function upgrade-macintosh-shit {
      echo "Checking if mf uninstalled xcode cli tools"
      if [[ $(brew config | grep 'CLT') == "CLT: N\A" ]]; then
        xcode-select --install
      fi
      echo "cleanup older stuff [current becomes old]"
      brew cleanup
      echo "updating brew itself"
      brew update
      echo "updating brew stuff"
      brew upgrade --formula
      echo "updating cask"
      brew upgrade --cask
      echo "updating mas apps"
      mas upgrade
      #omz update

      #echo "installing apps that need to be addressed specifically"
      # brew install --cask tailscale
      # brew install --cask telegram

      echo "check for outdated"
      brew outdated

      echo "If formulas appear up here, it means you should run upgrade-macintosh-greedy"
    }

    function upgrade-macintosh-greedy {
      echo "Checking if mf uninstalled xcode cli tools"
      if [[ $(brew config | grep 'CLT') == "CLT: N\A" ]]; then
        xcode-select --install
      fi
      echo "updating brew itself"
      brew update


      echo "check for outdated"
      # could do this but then no blacklist
      #brew upgrade --formula --greedy

      BLACKLIST=(
        "alfred"
        "1password"
        "devonthink"
        "dropbox"
        "google-drive"
        "istat-menus"
        "little-snitch"
        "microsoft-edge"
        ) #FIXME remove edge

      for item in $(brew outdated --cask --greedy | awk '{print $1}'); do

        if [[ ! " ${BLACKLIST[*]} " == *" ${item} "* ]]; then
            brew upgrade --cask --greedy ${item}
        fi
      done

      echo "check for outdated"
      brew outdated --greedy
    }

    function upgrade-macintosh-dryrun {

      echo "[dry-run] Checking if mf uninstalled xcode cli tools"
      if [[ "$(brew config | grep 'CLT')" == "CLT: N/A" ]]; then
        echo "[dry-run] would install xcode stuff"
      fi

      echo "[dry-run] updating brew stuff"
      brew upgrade --formula -n

      echo "[dry-run] updating cask"
      brew upgrade --cask -n

      echo "installing apps that need to be addressed specifically"
      brew install --cask -n tailscale

      # echo "updating mas apps"
      # mas upgrade
      # #omz update
    }

    #small wrapper for anybar
    function anybar() {
      if [[ -z ${1} ]]; then
        echo ""
        echo "Error! possible values are:"
        echo ""
        echo "white
red
orange
yellow
green
cyan
blue
purple
black
question
exclamation
filled
hollow

quit" && return 1
      fi

      #TODO: if anybar isnt running start it
      # osascript -e "tell application 'System Events'
      #                 if (get name of every application process) contains 'Shades' then
      #                   display 'wowzee'
      #                 end if
      #               end tell"
      # open -a anybar # add -n for more instances
      # ANYBAR_INIT=red   ANYBAR_PORT=1738 ANYBAR_TITLE=First  open -na AnyBar
      # ANYBAR_INIT=green ANYBAR_PORT=1739 ANYBAR_TITLE=Second open -na AnyBar
      # ANYBAR_INIT=blue  ANYBAR_PORT=1740 ANYBAR_TITLE=Third  open -na AnyBar

      echo -n ${1} | nc -4u -w0 localhost ${2:-1738};
      #osascript -e "tell application 'AnyBar' to set image name to '${1}'"
    }

    #NB REMEMBER: MAC GREP IS NOT GNU GREP (in general, but i have changed it)
    function highlight() { # from https://stackoverflow.com/questions/981601/colorized-grep-viewing-the-entire-file-with-highlighted-matches
        egrep --color "${1}|$" # ${2} #TODO check if leaving the $2 allows to both use the pipe AND the function as grep
    }
    alias catlight='highlight'

    # https://stackoverflow.com/questions/11454343/pipe-output-to-bash-function
    # function highlight(){
    #     grep --color -E '\${1}|$'
    # }
fi

#sed '/ubuntucazzo/{N;N;d;}' testfileSED # delete pattern and next 2 occurrences


#run a command in a detached window (creating a pane) of tmux. from http://blog.sanctum.geek.nz/watching-with-tmux/
function tmpane () { tmux split-window -dh "$*" ; }
function tmw () { tmux new-window -d "$*" ; }

#update only if we are in tmux, otherwise spawn a session and do it
function tmupgrade () {
    if [[ -z "$TMUX" ]]; then
        tmux new-session -d -sUpgrade "print '\n \n \n Beginning package update \n';sudo apt-get upgrade || read nothing\?pause"
    else
        tmux new-window -d -nUpgrade "print '\n \n \n Beginning package update \n';sudo apt-get upgrade || read nothing\?pause"
    fi
}

#possibly useless
function simulate-update { sudo apt-get update && sudo apt-get upgrade -s }

#find definition on dictionary
function define () {curl dict://dict.org/d:$* }

#find definition on CS dictionary
function defineCS () {curl dict://dict.org/d:$*:foldoc }

#open dash (macOS) to a specific definition (usage: dash docker expose; dash ansible vault..)
function dash () {open dash://${1}:${2} }

#Cave Johnson quote!
#note, the site has closed :/ maybe get the sentences from the site and put into fortune
#https://www.theportalwiki.com/wiki/Cave_Johnson_voice_lines
function cavejohnson { curl -s http://www.cavejohnsonhere.com/random/ | sed -nr 's|.*quote_main">(.*)|\n\1|p'}

#changed from alias to function, ifconfig (it is a mess in Debian)
function ifup-fab () {
    sudo ip link set ${1} up
}
function ifdown-fab () {
    sudo ip link set dev ${1} down
}

#Support function for changenetwork
function networkfilesCreation {

      echo;
      echo "There are neither home nor away profiles defined for network";
      echo;
      echo "It has been created now; check its correctness";

      sudo bash -c 'echo "
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet static
        address 192.168.5.4
        netmask 255.255.255.0
        gateway 192.168.5.1
" >> /etc/network/interfaces.home';

      sudo bash -c 'echo "
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet dhcp
" >> /etc/network/interfaces.away';

      echo "HOME";
      cat /etc/network/interfaces.home;
      echo;
      echo "AWAY";
      cat /etc/network/interfaces.away;
      echo;
}

#Support function for changenetwork
function resolvfileCreation {

    echo;
    echo "There are neither home nor away profiles defined for resolv.conf";
    echo;
    echo "It has been created now; check its correctness";
    sudo bash -c 'echo "nameserver 192.168.5.1" >> /etc/resolv.conf.home';
    sudo bash -c 'echo "nameserver 192.168.1.1" >> /etc/resolv.conf.away';
    echo "HOME";
    cat /etc/resolv.conf.home;
    echo;
    echo "AWAY";
    cat /etc/resolv.conf.away;
    echo;
}

#Change network to home (static IP) or away (DHCP)
function changenetwork () {

    #check if Xorg is installed. If it is, it's very likely networkmanager is installed
    #and it messes up the configuration.
    if [[ -e "/usr/bin/Xorg" ]]; then
      echo "Xorg is installed! use networkmanager to set IPs, because anyway it won't work.";
      exit;
    fi


    #first check that we created the files needed (.home and .away)
    # stupid jolly [ha]* does not work...
    if [[ ! -e "/etc/network/interfaces.home" || ! -e "/etc/network/interfaces.away" ]]; then
      networkfilesCreation;
    fi

    if [[ ! -e "/etc/resolv.conf.home" || ! -e "/etc/resolv.conf.away" ]]; then
      resolvfileCreation;
    fi

    #change to home
    if [[ ${1} == "--home" ]]; then
      sudo rm /etc/resolv.conf && sudo ln -s /etc/resolv.conf.home /etc/resolv.conf
      sudo rm /etc/network/interfaces && sudo ln -s /etc/network/interfaces.home /etc/network/interfaces
    fi

    #change to away
    if [[ ${1} == "--away" ]]; then
      sudo rm /etc/resolv.conf && sudo ln -s /etc/resolv.conf.away /etc/resolv.conf
      sudo rm /etc/network/interfaces && sudo ln -s /etc/network/interfaces.away /etc/network/interfaces
    fi

    #reset of the interface
    #sudo ip link set dev eth0 down
    #sleep 3
    #sudo ip link set eth0 up
    ifdown-fab eth0
    sleep 3
    ifup-fab eth0

}

function ausweather
{
curl -s http://open.live.bbc.co.uk/weather/feeds/en/3176219/3dayforecast.rss | awk -F'</*title>' '!a[$2]++&&NF!=1 {gsub("&#xB0;","",$2); print $2}';
}

function wthr()
{
    curl wttr.in/"${1}"
}

function weather(){
  wthr "${1}"
}

#for now just a stupid alias
function ytcli()
{
    mpsyt ${1}
}

function ytdl()
{
    /usr/local/bin/youtube-dl -x --audio-format mp3 --audio-quality 128K -o "~/Music/ytdl/%(title)s.%(ext)s" ${1}
}

function ytdlvid()
{
    /usr/local/bin/youtube-dl -x --audio-format mp3 --audio-quality 128K -o "~/Music/ytdl/%(title)s.%(ext)s" -k ${1}
}

function ytdllist()
{
    if [[ -s ${1} ]]; then
        /usr/local/bin/youtube-dl -x --audio-format mp3 --audio-quality 128K -o "~/Music/ytdl/%(title)s.%(ext)s" -a "${1}" >> ~/Music/thelog.txt
    fi
}

#Various functions unused

# awk aliases
alias -g A1="| awk '{print \$1}'"
#whats |& http://askubuntu.com/questions/24953/using-grep-with-pipe-and-ampersand-to-filter-errors-from-find
#(it means Error messages are written to stderr, not stdout. |& redirects stderr)
#2> /dev/null redirects stderr to /dev/null
alias -g EA='|& awk '
alias -g EA1="|& awk '{print \$1}'"

##functions
# find a random excuse
function excuse
{
    nc bofh.jeffballard.us 666 | tail -1
}

# Create a directory like "year-month-day" (i. e. 2007-07-16)
function mdate
{
    mkdir `date +%F`
    cd `date +%F`
}

# A nicer output of cal(1)
# MISC: Colorize the output of cal(1)
function calendar {
    if [[ ! -f /usr/bin/cal ]] ; then
        echo "Please install cal before trying to use it!"
        return
    fi
    if [[ "$#" = "0" ]] ; then
        /usr/bin/cal | egrep -C 40 --color "\<$(date +%e| tr -d ' ')\>"
    else
        /usr/bin/cal $@ | egrep -C 40 --color "\<($(date +%B)|$(date +%e | tr -d ' '))\>"
    fi
}

# eheheh :>
# MISC: lets sing ;-)
function littlelamb() {
    beep -f 10
    if [ $? = 0 ]; then
        echo 'Marry Had A Little Lamb'
        beep -f 466.2 -l 250 -D 20 -n -f 415.3 -l 250 -D 20 -n -f 370.0 -l 250 -D 20 -n -f 415.3 -l 250 -D 20 -n -f 466.2 -l 250 -r 2 -d 0 -D 20 -n -f 466.2 -l 500 -n -f 10 -l 20
        echo 'Little Lamb, Little Lamb'
        beep -f 415.3 -l 250 -r 2 -d 0 -D 20 -n -f 415.3 -l 500 -D 20 -n -f 466.2 -l 250 -D 20 -n -f 568.8 -l 250 -D 20 -n -f 568.8 -l 500 -n -f 10 -l 20
        echo 'Marry Had A Little Lamb'
        beep -f 466.2 -l 250 -D 20 -n -f 415.3 -l 250 -D 20 -n -f 370.0 -l 250 -D 20 -n -f 415.3 -l 250 -D 20 -n -f 466.2 -l 250 -r 2 -d 0 -D 20 -n -f 466.2 -l 250 -n -f 10 -l 20
        echo 'Whose Fleece Was White As Snow'
        beep -f 415.3 -l 250 -r 3 -D 20 -n -f 466.2 -l 250 -D 20 -n -f 415.3 -l 250 -D 20 -n -f 370.0 -l 500
fi
}

# MISC: Display current directory as a 'tree'.
if [ ! -x  $(which tree > /dev/null 2>&1) ]; then
    function tree() { find . | sed -e 's/[^\/]*\//|----/g' -e 's/---- |/    |/g' | $PAGER }
fi

# MISC: Making the right decisions is hard :>
function helpme
{
    print "Please wait.. I'll think about it"
    for i in 1 2 3; do echo -ne "."; sleep 0.3; done
    if [ $RANDOM -gt $RANDOM ]
    then
        print "Yes\!"
    else
        print "No\!"
    fi
}

# PROG: colorizing the output of make
if [[ -x ~/bin/makefilter ]]
then
    make() { command make "$@" |& makefilter }
fi

## MISC: clean directory
#function purge()
#{
#   FILES=(*~(N) .*~(N) \#*\#(N) *.o(N) a.out(N) *.core(N) *.cmo(N) *.cmi(N) .*.swp(N))
#   NBFILES=${#FILES}
#   if [[ $NBFILES > 0 ]]; then
#       print $FILES
#       local ans
#       echo -n "Remove this files? [y/n] "
#       read -q ans
#       if [[ $ans == "y" ]]
#       then
#           command rm ${FILES}
#           echo ">> $PWD purged, $NBFILES files removed"
#       else
#           echo "Ok. .. than not.."
#       fi
#   fi
#}

## MISC: set the DISPLAY to where i'm logged from or - if an #argument is specified - to the value of the argument
#function disp()
#{
#        if [[ $# == 0 ]]
#        then
#                DISPLAY=$(who am i | awk '{print $6}' | tr -d '()'):0
#        else
#                DISPLAY="${*}:0"
#        fi
#        export DISPLAY
#}

#if [ ! -x $(which unp) >/dev/null 2>&1 ]; then
## ARCHIVE: extracts archived files (maybe)
#function simple-extract ()
#{
#        if [[ -f "$1" ]]
#        then
#                case "$1" in
#                        *.tar.bz2)  bzip2 -v -d "$1" ;;
#                        *.tar.gz)   tar -xvzf "$1"   ;;
#                        *.ace)      unace e "$1"     ;;
#                        *.rar)      unrar x "$1"     ;;
#                        *.deb)      ar -x "$1"       ;;
#                        *.bz2)      bzip2 -d "$1"    ;;
#                        *.lzh)      lha x "$1"       ;;
#                        *.gz)       gunzip -d "$1"   ;;
#                        *.tar)      tar -xvf "$1"    ;;
#                        *.tgz)      gunzip -d "$1"   ;;
#                        *.tbz2)     tar -jxvf "$1"   ;;
#                        *.zip)      unzip "$1"       ;;
#                        *.Z)        uncompress "$1"  ;;
#                        *.shar)     sh "$1"          ;;
#                        *)          echo "'"$1"' Error. Please go away" ;;
#                esac
#        else
#                echo "'"$1"' is not a valid file"
#        fi
#}
#fi

# ARCHIVE: Create a tarball from given directory
# function create-archive()
# {
#     local archive_name
#     archive_name="$1.tar.gz"
#     archive_name=${archive_name/\//}
#     tar cvfz "$archive_name" "$1"
#     echo "Created archive $archive_name"
# }
# compdef _directories create-archive


# ARCHIVE: Only needed if lesspipe unavailable
#if [ ! -x $(which lesspipe.sh > /dev/null 2>&1)  ]; then
#   # view archive without unpack
#   function show-archive()
#   {
#       if [[ -f $1 ]]
#       then
#           case $1 in
#               *.tar.gz)      gunzip -c $1 | tar -tf - -- ;;
#               *.tar)         tar -tf $1 ;;
#               *.tgz)         tar -ztf $1 ;;
#               *.zip)         unzip -l $1 ;;
#               *.bz2)         bzless $1 ;;
#               *)             echo "'$1' Error. Please go away" ;;
#           esac
#       else
#           echo "'$1' is not a valid archive"
#       fi
#   }
#fi

# MISC: Convert gif's to png's
#function gif2png()
#{
#   if [[ $# = 0 ]]
#   then
#       echo "Usage: $0 foo.gif"
#       echo "Purpose: change a GIF file to a PNG file"
#   else
#       output=`basename $1 .gif`.png
#       convert  $1 $output
#       touch -r $1 $output
#       ls -l $1 $output
#   fi
#}

# MISC: add a "load-level"
function load()
{

    LOAD=$(uptime | awk '{ print $11 }')
    if [[ $LINUX == "true" ]]; then
        ((Lev=$LOAD/$(nproc)))
    else
        ((Lev=$LOAD/$(sysctl -n hw.ncpu)))
    fi
    case $Lev {
        0.0*)     llevel="relax.."   ;;
        0.[123]*) llevel="normal."   ;;
        0.[456]*) llevel="verspannt"   ;;
        0.[789]*) llevel="WTF?!"  ;;
        1*)       llevel="dangerous!" ;;
        2*)       llevel="HELP ME!!!111!"   ;;
        [3-9]*)   llevel="He's dead jim!"   ;;
        *)        return         ;;
    }
    echo "$llevel"
}
# MISC: display some informations
function status()
{
    # I must use this because i use this function on different
    # systems
        local system="$(uname -sr)"

        print ""
        print "Date  : "$(date "+%Y-%m-%d %H:%M:%S")""
        print "Shell : Zsh $ZSH_VERSION (PID = $$, $SHLVL nests)"
        print "Term  : $TTY ($TERM), $BAUD bauds, $COLUMNS x $LINES cars"
        print "Login : $LOGNAME (UID = $EUID) on $HOST"
        print "System: $system"
        #print "Uptime:$(uptime)"
        print "Uptime: $(print ${${$(=uptime)[3,5]}:gs/,//})"
        print "Load  : "$(load)""
        print ""

    which archey > /dev/null
    if [[ $? ]]; then
        archey
    fi
}

# MISC: Display ``status'' after "the first shell"
# if [[ $SHLVL -eq 1 ]] ; then
#     status
# fi

if [[ -e bin/z/z.sh ]]; then
    source bin/z/z.sh
    export _Z_CMD=zz
fi

#archbey script, maybe too much if added to the MOTD and status above
##!/bin/bash
## Archbey2 by Mr Green
## v 0.06

#colour="\e[01;36m" # Green
#os_string="ArchBang"
#host_name=$(hostname)
#kernel=$(uname -r)
#arch_bit=$(uname -m)
#up_time=$(uptime | awk '{print $3}' | sed 's|,||g')
#wm=$DE
#num_packs=$(pacman -Qq | wc -l)
#cpu_name=$(grep 'model name' /proc/cpuinfo  | uniq | awk -F':' '{print $2}' | cut -f1 -d"@")
#ram_total=$(free -mto | grep Mem: | awk '{print $2}')
#ram_used=$(free -m | grep "buffers/cache" | awk '{print $3}')
#my_shell=$(echo $SHELL | sed 's|/bin/||g')
##df -h --output=fstype,size,used /home | tail -n1 | awk '{ print $3" / "$2" ("$1")"}'
#root_space=$(df -h --output=fstype,size,used / | tail -n1 | awk '{ print $3" / "$2" ("$1")"}')
##home_space=$(df -h --output=fstype,size,used $HOME | tail -n1 | awk '{ print $3" / "$2" ("$1")"}')

#echo
#echo -e "${colour}                                OS:\e[00m ${os_string} ${arch_bit}"
#echo -e "${colour}               /\               Hostname:\e[00m ${host_name}"
#echo -e "${colour}              .;#.              Kernel:\e[00m ${kernel}"
#echo -e "${colour}             /####\             Uptime:\e[00m ${up_time}"
#echo -e "${colour}            ;##   #;            Window Manager:\e[00m ${wm}"
#echo -e "${colour}           +###  .##            Packages:\e[00m ${num_packs}"
#echo -e "${colour}          +####  ;###           RAM:\e[00m ${ram_used} / ${ram_total} MB"
#echo -e "${colour}         ######  #####;         CPU:\e[00m${cpu_name}"
#echo -e "${colour}        #######  ######         Shell:\e[00m ${my_shell}"
#echo -e "${colour}       ######## ########        Root:\e[00m ${root_space}"
#echo -e "${colour}     .########;;########;       "
#echo -e "${colour}    .########;   ;#######       "
#echo -e "${colour}    #########.   .########;     "
#echo -e "${colour}   ######'           '######    "
#echo -e "${colour}  ;####                 ####;   "
#echo -e "${colour}  ##'                     '##   "
#echo -e "${colour} #'                         '#  \e[00m"
#echo



