#!/bin/zsh

### setup part
FAB_KUBE=true #used in main zshrc
GCP_PROJECT=jobtome-example
GITLAB_TOKEN=my-gitlab-token
GCLOUDSDK_PATH="${HOME}/.google-cloud-sdk/bin"
###

export PATH="${GCLOUDSDK_PATH}:${PATH}"

[ -f "${filesLocation}"/kubectl_aliases.zshrc ] && source "${filesLocation}"/kubectl_aliases.zshrc

source <(kubectl completion zsh)
which helm > /dev/null && source <(helm completion zsh)

function google_k8s_login(){
    gcloud container clusters get-credentials internal --zone europe-west1-b --project ${GCP_PROJECT}
    gcloud container clusters get-credentials production-asia-east1 --zone asia-east1-a --project ${GCP_PROJECT}
    gcloud container clusters get-credentials production-europe-west1 --zone europe-west1-b --project ${GCP_PROJECT}
    gcloud container clusters get-credentials production-us-central1 --zone us-central1-a --project ${GCP_PROJECT}
    gcloud container clusters get-credentials quality --zone europe-west1-b --project ${GCP_PROJECT}
    #sed to change context names?
}

# #Google cloud stuff
# # The next line updates PATH for the Google Cloud SDK.
# if [ -f '~/google-cloud-sdk/path.zsh.inc' ]; then . '~/google-cloud-sdk/path.zsh.inc'; fi
# # The next line enables shell command completion for gcloud.
# if [ -f '~/google-cloud-sdk/completion.zsh.inc' ]; then . '~/google-cloud-sdk/completion.zsh.inc'; fi

# #Google cloud utils (???) stuff
# # The next line updates PATH for the Google Cloud SDK.
# if [ -f '/Users/fbel/google-cloud-sdk-gsutils/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/fbel/google-cloud-sdk-gsutils/google-cloud-sdk/path.zsh.inc'; fi
# # The next line enables shell command completion for gcloud.
# if [ -f '/Users/fbel/google-cloud-sdk-gsutils/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/fbel/google-cloud-sdk-gsutils/google-cloud-sdk/completion.zsh.inc'; fi

# take secret in B64 and copy to the clipboard
function kubesecretb64(){
    base64 -i secrets.yaml | pbcopy
}

# Forgot how it's meant to be used LOL
function kubefwd(){
#e.g. kubectl port-forward service/postgres-service 8888:5432 -> connect to localhost:8888 for the service at port 5432
    kubectl port-forward "${2}" "${1}"
}

# Get cluster credentials (TODO: could be used above too, at init phase)
function kubegetclustercred(){ #kubegetclustercred europe-west1-b quality => cluster gke_jobtome-platform_europe-west1-b_quality
    gcloud container clusters get-credentials --zone "${1}" "${2}"
}

# Make a secret from a file (the whole file is the secret)
# NB: change NAME and NAMESPACE of the yaml once created
function kubemakesecretfromfile(){ #kubemakesecretfromfile filename
    kubectl create secret generic NAME --dry-run=client --type=Opaque -o yaml --from-file="${1}" --namespace=NAMESPACE >> secrets.yaml
    echo "---" >> secrets.yaml
}

# Make a secret from a file (the keys within the file are the secrets)
# NB: change NAME and NAMESPACE of the yaml once created
function kubemakesecretenvfromfile(){ #kubemakesecretenvfromfile filename
    kubectl create secret generic NAME --dry-run=client --type=Opaque -o yaml --from-env-file="${1}" --namespace=NAMESPACE >> secrets.yaml
    echo "---" >> secrets.yaml
}

# Make a cm from a file (the whole file is the secret)
# NB: change NAME and NAMESPACE of the yaml once created
function kubemakecmfromfile(){ #kubemakecmfromfile filename
    kubectl create configmap NAME --dry-run=client -o yaml --from-file="${1}" --namespace=NAMESPACE >> configmap.yaml
    echo "---" >> configmap.yaml
}

# Make a cm from a file (the keys within the file are the keys)
# NB: change NAME and NAMESPACE of the yaml once created
function kubemakecmfromenvfile(){ #kubemakecmfromenvfile filename
    kubectl create configmap NAME --dry-run=client -o yaml --from-env-file="${1}" --namespace=NAMESPACE >> configmap.yaml
    echo "---" >> configmap.yaml
}

# Get a token from gitlab and make it a secret for imagePullSecret
# NB: change NAMESPACE of the yaml once created
function kubemakecredentialssecret(){ #kubemakecredentialssecret user pass
    kubectl create secret docker-registry registry-credentials --docker-server=https://registry.jobtome.io/v2/ --docker-username="${1}" --docker-password="${2}" --docker-email=accounts@jobtome.com --dry-run=client -o yaml  --namespace=NAMESPACE >> secrets.yaml
}

# Force resource recreation by editing metadata (trick):
# after adding the annotation, the controller (e.g. deployment) will create another set
function kupdate { #kupdate <deploymentname> OR kupdate statefulset <statefulsetname>
    local resource=${1}
    local name=${2}

    if [ -z "${2}" ]; then
        name=${1}
        resource="deployment"
    fi

    kubectl patch "${resource}" "${name}" -p \
        "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"kupdate/date\":\"`date +'%s'`\"}}}}}"
}

# Forces a deploy to run in heavyloads nodes
function kheavy { #kheavy <deploymentname>
    local resource=${1}
    local name=${2}

    if [ -z "${2}" ]; then
        name=${1}
        resource="deployment"
    fi

    kubectl patch "${resource}" "${name}" --patch \
        '{"spec": {"template": { "spec": {"tolerations": [{"operator": "Equal", "key": "hardware", "value": "expensive", "effect": "NoSchedule"}]}}}}'
}

# Apply a manifest in all clusters (INCLUDING internal)
function apply_all_clusters(){
    echo "waiting 5s - usage is apply_all_clusters <filename>"
    sleep 5;
    CLUSTERS=("quality" "internal" "production-europe" "production-asia" "production-us" )
    for CLU in "${CLUSTERS[@]}"; do
        kubectl apply -f "${1}" --context="${CLU}"
        sleep 2s
        echo "applied ${1} in cluster ${CLU}"; # (if relevant, namespace $(kubens)";
    done
    # go to a 'safe' cluster afterwards
    kubectx quality
}

# Do something arbitrary in all clusters (INCLUDING internal)
function exec_all_clusters(){
    echo "waiting 5s - usage is exec_all_clusters <command>"
    sleep 5;
    CLUSTERS=("quality" "internal" "production-europe" "production-asia" "production-us" )
    for CLU in "${CLUSTERS[@]}"; do
        kubectx "${CLU}";
        #echo "$@"
        "${@}"
        sleep 2s
        echo "'${*}' has run in cluster ${CLU}";
    done
    # go back to a 'safe' cluster afterwards
    kubectx quality
}

# Set the same ns in all cluster (excluding internal because there's no guarantee that there;s same name)
function ns_all_clusters(){

    CLUSTERS=("quality" "production-europe" "production-asia" "production-us" )
    for CLU in "${CLUSTERS[@]}"; do
        kubectx "${CLU}";
        kubens "${1}"
        sleep 2s
        echo " =>> ns '${1}' set in cluster ${CLU}";
    done
    kubectx quality
    k config get-contexts  #kclusternames
}

# Apply a manifest replacing the placeholder for tag with the actual tag
# Used for k8s deployments, where some labels are awaiting to be envsubst'ed
# and it makes k8s complain of invalid value
function kanotag(){
    sed "s/\${CI_COMMIT_TAG}/$(git rev-parse --short=8 HEAD)/" "${1}" | ka -
}

alias kclusternames='k config get-contexts '

# Tool k9s
alias k9s='k9s -c dp' #start k9s showing deployments
alias k9ns='k9s -c ns' #start k9s showing namespaces
alias k9ctx='k9s -c ctx' #start k9s showing contexts

# Debugging utils (creates and connects to a pod in
# the namespace, to run operations for network testing for instance)
alias kdebugger='kubectl run kdebug --image=ubuntu -it -- bash'
alias kudebug="kubectl run --rm utils -it --generator=run-pod/v1 --image arunvelsriram/utils bash"
alias kudebuggrpc= "kubectl run --rm utils -it --generator=run-pod/v1 --image jobtomelabs/grpc-health-probe:1.0.0"

#kudebug() {
#    local image=gdiener/sysadmin-toolchain:v1.0.0
#
#    kubectl run -it debug-gdie \
#        --restart=Never \
#        --rm=true \
#        --image="${image}" -- "${@}"
#}

