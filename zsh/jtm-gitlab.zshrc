#!/bin/zsh

# I do this because I source gitlab.sh from a 'main' file
#  and at the end of this main file i also import custom.zshrc
#  which overwrites this variable
GITLAB_TOKEN=my-gitlab-token
GCP_PROJECT=jobtome-platform


function gitlab_help(){
    help_gitlab
}

function help_gitlab(){
    echo ""
    echo "the functions are:"
    echo "  makeSAkey"
    echo "  add_variable_in_gitlab"
    echo "  setup_gitlab_project"
    echo ""
}

# Take something (e.g. the json above) and put it in a variable
# (e.g. GOOGLE_KEY_XXX as per our standard) in gitlab

#  usage: add_variable_in_gitlab Varname VarValue [gitlabURL]

#   when gitlabURL omitted, take it from the current repo (current folder)
#   --> MAKE SURE you are in the correct folder. The script will ask you confirmation
## MOST IMPORTANT: the URL must be in git@ form, ending in .git
## also REMEMBER that if you are adding a google key, the SA will have " (double quotes) inside. so surround it with ' (single quotes)
function add_variable_in_gitlab {
    VAR_NAME=$1
    VAR_CONTENT=$2

    get_project_from_folder

    echo "Adding ${VAR_NAME}(=> '${VAR_CONTENT}') into ${GITLAB_PROJECT}"
    echo
    echo
    curl --request POST --header "Private-Token: ${GITLAB_TOKEN}" "https://git.jobtome.io/api/v4/projects/${GITLAB_PROJECT}/variables" --form "key=${VAR_NAME}" --form "value=${VAR_CONTENT}" --form "protected=true"
    echo
    echo
    echo

}

# Make the project compliant to our standard (and make it possible to see the
#  protected variables above): protect branches and tags, and create the deploy token
#  usage: setup_gitlab_project [gitlabURL]
#   when gitlabURL omitted, take it from the current repo
function setup_gitlab_project {

    # Initialise because if somebody sets up a project,
    # does cd anotherfolder/, and sets up another
    # project then there would be biiig failure
    unset GITLAB_PROJECT

    get_project_from_folder

    echo "porcodio1"
    echo ${GITLAB_PROJECT}
    echo
    echo
    echo

    # Protect branches and tags
    protect_branches_and_tags 

    echo "porcodio2"
    echo ${GITLAB_PROJECT}
    echo
    echo
    echo

    # Add variables (use function above) WIP

    # Create deploy token and put in the pasteboard
    # Put the pasteboard into a kubemakesecret, or tell the user to run kubemakesecret
    put_deploy_token_in_vars


    echo "porcodio3"
    echo ${GITLAB_PROJECT}
    echo
    echo
    echo
    
}

function protect_branches_and_tags {
  get_project_from_folder

  echo "Protect branches"
  curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://git.jobtome.io/api/v4/projects/${GITLAB_PROJECT}/protected_branches?name=master&push_access_level=0&merge_access_level=30&unprotect_access_level=40"

  echo
  echo
  echo

  echo "Protect tags"
  curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://git.jobtome.io/api/v4/projects/${GITLAB_PROJECT}/protected_tags?name=v*&create_access_level=40"

  echo
  echo
  echo
}

function create_deploy_token {
  get_project_from_folder
  # Create deploy token and put in the pasteboard
  echo "Create deploy token"
  curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" --header "Content-Type: application/json" --data '{"name": "registry-credentials", "scopes": ["read_registry"]}' "https://git.jobtome.io/api/v4/projects/${GITLAB_PROJECT}/deploy_tokens/" | jq -r '[.username, .token] | join(" ")' | pbcopy
}

function put_deploy_token_in_vars {
  create_deploy_token
  CLIP=$(pbpaste)
  if [[ ${RIGHT_FOLDER} == "1" ]]; then
    echo "Creating kube thing"
    kubemakecredentialssecret "${CLIP}"
  else
    echo "run 'kubemakecredentialssecret ${CLIP}' in the project folder"
  fi
}

function get_project_from_folder { #FIXME do i want to give chance to add project from another folder?

  # NOTE: the git remote MUST be in ssh format
  # because the grep will pick everything between : and .git

  if [[ -z ${GITLAB_PROJECT} ]]; then #only ask confirmation the first time
    echo
    echo "are you in the right folder??"
    echo

    URL=$(git remote -v | grep origin | grep push | awk '{print $2}')
    
    # make URL in ssh format
    URL=$(echo ${URL} | sed "s|https://git.jobtome.io/|git@git.jobtome.io:|" )

    read "choice?WARNING, do you want ${URL} to be the repo? (y/n)?" #FIXME this works in zsh but not bash

    case "${choice}" in
      y|Y ) RIGHT_FOLDER=1; echo "pinocchietto";;
      n|N ) return 2;;
      * ) echo "invalid" && return 1;;
    esac

    GITLAB_PROJECT=$(echo "${URL}" | grep -Eo ':([0-9a-zA-Z\/\_\-]*)\.git' | sed 's|:||' | sed 's|.git$||' | sed 's|/|%2F|g' )

  fi

}

# nice-to-have:
#  - a method that chains all the above, e.g. 'createproject' (SEE AUTOMIN?)
#  - a method that checks what still needs to be done, e.g. "SA has permission, variables are set, but the branch is not protected"

# function project_status {

#   get_project_from_folder #local shell
#   check_branch_protection #gitlab
#   check_variables_and_their_content #gitlab
#   check_sa_existence_and_permissions #gcp

# }


############
