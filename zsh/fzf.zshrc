#!/bin/zsh
# fzf.zsh sets options for fzf

#more interesting stuff: https://github.com/junegunn/fzf/wiki/Examples

#options for fuzzy history search
export FZF_CTRL_R_OPTS='--sort --exact'

# Setup fzf
# ---------
if [[ ! "$PATH" == */usr/local/opt/fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/usr/local/opt/fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/usr/local/opt/fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
if [[ "$(uname)" == "Darwin" ]]; then
  source "$(brew --prefix)/opt/fzf/shell/key-bindings.zsh"
else
  source "/usr/bin/opt/fzf/shell/key-bindings.zsh"
fi

# fd - cd to selected directory
fd() {
  local dir
  dir=$(find ${1:-.} -path '*/\.*' -prune \
                  -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}

# cdf - cd into the directory of the selected file
cdf() {
  local file
  local dir
  file=$(fzf +m -q "$1") && dir=$(dirname "$file") && cd "$dir"
}
