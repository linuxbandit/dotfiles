#!/bin/zsh

# Checks if a SA exists by listing all of them and grepping
function sa-exists(){

  if [[ -z ${1} ]]; then
    echo "Checks if a SA exists by listing all of them and grepping"
    echo ""
    echo "usage: sa-exists <service-account-name>"
  else
    gcloud iam service-accounts list | grep "${1}"
  fi

}

# SA is already made with TF, now generate the key and
#  have the json in the clipboard
function makeSAkey {

  if [[ -z "${1}" ]]; then
    echo "Usage: makeSAkey <serviceaccount name before the '@'> (and the json will magically be in your clipboard)"
  else

    SERVICE_ACCOUNT="${1}@${GCP_PROJECT}.iam.gserviceaccount.com"

    gcloud iam service-accounts describe ${SERVICE_ACCOUNT} --project ${GCP_PROJECT} || { echo "serviceaccount not created! cannot generate key"; return 1 }

    gcloud iam service-accounts keys create /tmp/key.json --iam-account=${SERVICE_ACCOUNT} --project ${GCP_PROJECT}

    cat /tmp/key.json | pbcopy && rm /tmp/key.json
  fi

}

function put-on-cdn(){
  if [[ -z ${1} || -z ${2} ]]; then
    echo "Puts something (assumed to be in ~/Downloads) on cdn.jobtome.com"
    echo ""
    echo "usage: put-on-cdn <filename> <dirname>"
    echo "example expanded:"
    echo "  put-on-cdn (~/Downloads/)filename.xml dirname"
    echo "  -> gs://cdn.jobtome.com/dirname/filename.xml"
  else
    set -x
    gsutil cp ~/Downloads/${1} gs://cdn.jobtome.com/${2}/${1}
    set +x
  fi
}

function hotfix(){
  if [[ -z ${1} ]]; then
    echo "Error: branch name not specified"
    echo
  else
    git checkout -b hotfix/${1}
  fi
}

function setup-helm(){
  #read -p HELM_DEPLOYMENTS_FOLDER
  #git clone https://git.jobtome.io/devops/helm-charts/helm-deployments ${HELM_DEPLOYMENTS_FOLDER}
  #in the end it will export HELM_DEPLOYMENTS_FOLDER (currently inside the custom.zshrc)
}
function setup-automin(){
  #read -p AUTOMIN_FOLDER
  #git clone https://git.jobtome.io/devops/tools/automin ${AUTOMIN_FOLDER}
  #in the end it will export AUTOMIN_FOLDER (currently inside the custom.zshrc)
}

function helm-instructions(){
  make ${1} -f "${HELM_DEPLOYMENTS_FOLDER}/Makefile"
}

function automin(){
  make ${1} -f "${AUTOMIN_FOLDER}/Makefile"
}

function ansi-code-jtm() {
  if [[ ! -f ~/.vault_password ]]; then
    echo "You did not set the vault password! go to pass.jobtome.io and login, "
    echo "look for the password, and put it in ~/.vault_password "
    return 1
  fi
  ansible-vault encrypt --vault-password-file ~/.vault_password
}

function ansi-decode-jtm() {
  if [[ ! -f ~/.vault_password ]]; then
    echo "You did not set the vault password! go to pass.jobtome.io and login, "
    echo "look for the password, and put it in ~/.vault_password "
    return 1
  fi
  ansible-vault decrypt --vault-password-file ~/.vault_password
}
