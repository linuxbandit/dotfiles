#!/bin/zsh

#ALIASES
##ls, the common ones I use a lot shortened for rapid fire usage

if [[ $LINUX == "true" ]]; then
    alias ls='ls --color' #I like colour
else
    alias ls='ls -G' #I like colour
    alias clear_dns_cache='killall -HUP mDNSResponder'
fi

alias lsf="ls -l | egrep -v '^d'"  #shows only files
alias lsdir="ls -l | egrep '^d'"   #shows only dirs
alias ll='ls -lFh'     #size,show type,human readable
alias la='ls -lAFh'   #long list,show almost all,show type,human readable
alias lla='ls -la'   #long list,show almost all,show type,human readable
alias lr='ls -tRFh'   #sorted by date,recursive,show type,human readable
alias lt='ls -ltFh'   #long list,sorted by date,show type,human readable

alias t='tail -f -n 100'

alias dud='du -d 1 -h'
alias duf='du -sh *'
alias duff='/usr/local/bin/duf'

alias fdir='find . -type d -name'
alias ffil='find . -type f -name'

## Repurpose of various utils

# use bat instead of cat
alias cat='cat_or_bat'

#note: overridden by a grep function (see functions.zshrc)
alias grep="grep --colour"
alias checkwinlineending="egrep -l $'\r'\$ *"

if [[ $LINUX != "true" ]]; then
    #mac-specific aliases
    alias md5sum="md5"
    alias sha1sum="shasum"
fi

#see https://unix.stackexchange.com/questions/25327/watch-command-alias-expansion
alias watch="watch "

alias wtfutil="wtfutil --config=${HOME}/.dotfiles/wtf-config.yml"

#Connect to the session tmux <---note: session name given as param (e.g. grasshopper)
alias connect='tmux -2 attach-session -t'

alias compress='tar -zcvf'
alias uncompress='tar -zxvf'

alias treeview="ls -R | grep \":$\" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'"

alias rsync-move="rsync -avhP --exclude '.DS_Store' --remove-source-files"

## Shortnames of common programs

#GIT
alias ga='git add'
alias gaa='git add -A'
alias gph='git push'
alias gpu='git pull'
alias gpl='git pull'
alias gl='git log'
alias gs='git status'
alias gst='git staged'
alias gd='git diff'
alias gc='git commit -m'
#alias gma='git commit -am'
alias gb='git branch'
alias gco='git checkout'
alias gcob='git checkout -b'
alias gcom='git checkout master'
alias gupd='{git checkout master || git checkout stable } && git pull' #g update
alias gr='git remote'
alias gra='git remote add'
alias grr='git remote rm'
alias gcl='git clone'
alias gtag='git tag -a -m'
alias gpomm='git push origin master' # gpom would be too close to gcom
alias gpomm-t='git push origin master --tags' #you cant use an alias in an alias
alias gpobasitshouldbe='git push origin $(git branch --show-current) | grep "remote:\s*(https.*)" | pbcopy ' #you cant use an alias in an alias
#commented because not gitlab anymore alias gpob='git push origin $(git branch --show-current) | tee /tmp/gitlaburl && cat /tmp/gitlaburl | grep "remote:\s*(https.*)" | pbcopy' #you cant use an alias in an alias
alias gpob='git push origin $(git branch --show-current) '
alias prettylog='git log --pretty=oneline --abbrev-commit --graph --decorate'
#the aliases below are useful for ignoring changes to tracked files
alias gitign='git update-index --assume-unchanged' #<filename>
alias gitnoign='git update-index --no-assume-unchanged' #<filename>
alias gitwhichign="git ls-files -v|grep '^h'"
alias goprojroot='cd $(git rev-parse --show-toplevel 2>/dev/null || pwd )'
alias makeupstreamro='gr rename origin upstream; gr set-url upstream --push "DONTPUSH"'

#DOCKER
#alias dockrun='docker run -it' ## has been moved to text expansion in alfred
#compose
alias docker-compose='docker compose'
alias d-c='docker-compose'
alias dcu='d-c up'
alias dcd='d-c down'
alias dce='d-c exec'
alias docker-cleancache='docker builder prune'

#there are no other docker tags but latest....
alias dry='docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -e DOCKER_HOST=$DOCKER_HOST moncho/dry'

#TERRAFORM
alias tf='terraform'
alias tf-i='terraform init'
alias tf-p='terraform plan'
alias tf-pnc='terraform plan -no-color'
alias tf-a='terraform apply'
alias tf-d='terraform destroy'

## OTHER
# Make an ssh private key (RSA 4096bit)
alias generate-ssh-key='ssh-keygen -t rsa -b 4096 -C "$(whoami)@$(hostname)-$(date '+%Y-%m-%d')"'

# Make a certificate
#alias generate-certificate="openssl req -new -x509 -nodes -out cert.pem -keyout cert.key -days 365"

alias bigfun="cat /dev/urandom | hexdump -C | grep \"ca fe\""

alias p-cra='pre-commit run -a'

alias cdicloud='cd ~/Library/Mobile Documents/com~apple~CloudDocs'

alias archey='archey -l retro'
