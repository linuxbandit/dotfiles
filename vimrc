" Part of this configuration from amix.dk
" Part from my friends of OpenLab
" All the nice comments are from amix though!

"don't be backward-compatible with old vi
set nocompatible

set encoding=utf-8

" Sets how many lines of history VIM has to remember
set history=700

" Sets case ignoring when using search
set ignorecase
" Sets smart case matching
set smartcase

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","

" Set 7 lines to the cursor - when moving vertically using j/k
set scrolloff=7

"Always show current position
set ruler

" Height of the command bar
set cmdheight=2

" A buffer becomes hidden when it is abandoned
set hid

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

" Enable syntax highlighting
syntax enable

colorscheme desert
set background=dark

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Always show the status line
set laststatus=2

" Format the status line
" set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ %{fugitive#statusline()}\ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\,%c%V(%P)\ \ FormatOptions:%{&fo}\ FileFormat:%{&ff} 

"set textwidth=72
"git commit message as suggested by http://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message
autocmd Filetype gitcommit setlocal spell textwidth=72
set colorcolumn=+1

"The FormatOptions above (in the status line) and the piece below come from 
"about wrapping at 72 for good commit msg
"augroup vimrc_autocmds
"    autocmd BufEnter * highlight OverLength ctermbg=yellow guibg=#592929
"    autocmd BufEnter * match OverLength /\%72v.*/
"augroup END 
"^^Now it works but the line is nicer so it is commented out

"pressing ,fr will format to respect the 72 lines (if not automatically
"happening)
map <leader>fr gq<cr>


" Remap VIM 0 to first non-blank character
" map 0 ^

" Pressing ,occ will check how many occurrences of the word under the cursor
map <leader>occ :execute ":%s@\\<" . expand("<cword>") . "\\>\@&@gn"<CR>

" Pressing ,sc will toggle and untoggle spell checking
map <leader>sc :setlocal spell!<cr>

map <Leader>q" ciw""<Esc>P
map <Leader>q' ciw''<Esc>P

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction


"turn on line numbers
set number

" If fzf installed using Homebrew
set rtp+=/usr/local/opt/fzf

" Plugins
filetype off
set rtp+=~/.dotfiles/vimplugins/VundleVim
call vundle#begin()

source ~/.dotfiles/vimplugins/vundle.vim
source ~/.dotfiles/vimplugins/fugitive.vim
source ~/.dotfiles/vimplugins/vim-airline.vim
source ~/.dotfiles/vimplugins/ctrlp.vim
source ~/.dotfiles/vimplugins/nerdtree.vim

call vundle#end()

" Enable filetype plugins
filetype plugin on
filetype indent on
