#!/bin/bash

# SHELL stuff
if [[ -f  ~/.zshrc ]]; then rm ~/.zshrc; fi ; ln -s ~/.dotfiles/zshrc ~/.zshrc
if [[ -f  ~/.gitconfig ]]; then rm ~/.gitconfig; fi ; ln -s ~/.dotfiles/gitconfig ~/.gitconfig
if [[ -f  ~/.tmux.conf ]]; then rm ~/.tmux.conf; fi ; ln -s ~/.dotfiles/tmux.conf ~/.tmux.conf
if [[ -f  ~/.vimrc ]]; then rm ~/.vimrc; fi ; ln -s ~/.dotfiles/vimrc ~/.vimrc
if [[ -f  ~/.ssh/config ]]; then rm ~/.ssh/config; fi ; ln -s ~/.dotfiles/ssh-config ~/.ssh/config

git submodule init
git submodule update

#Now that vundle is installed (submodule),
# we can add the plugins
vim +PluginInstall +qall

if [[ -f  ~/.cig.yml ]]; then rm ~/.cig.yml; fi ; ln -s ~/.dotfiles/cig.yml ~/.cig.yml
if [[ -f  ~/.fzf.zshrc ]]; then rm ~/.fzf.zshrc; fi ; ln -s ~/.dotfiles/zsh/fzf.zshrc ~/.fzf.zshrc

#WM/Mac/GUI stuff
if [[ $(uname) != "Linux" ]]; then
  if [[ -f  ~/.yabairc ]]; then rm ~/.yabairc ; fi ; ln -s ~/.dotfiles/wm/yabairc ~/.yabairc;
  if [[ -f  ~/.phoenix.js ]]; then rm ~/phoenix.js ; fi ; ln -s ~/.dotfiles/phoenix/phoenix.js ~/phoenix.js;
  ./wm/install-limelight.sh

  mkdir -p "${HOME}/Library/Application Support/Sublime Text 3/Packages/User/" > /dev/null 2>&1
  # NB: if A is symlink to B and B doesn't exist, [[ -f A ]] is false
  if [[ -f  "${HOME}/Library/Application Support/Sublime Text 3/Packages/User/Preferences.sublime-settings" ]]; then rm "${HOME}/Library/Application Support/Sublime Text 3/Packages/User/Preferences.sublime-settings" ; fi ; ln -s "${HOME}/.dotfiles/editors/sublime.json" "${HOME}/Library/Application Support/Sublime Text 3/Packages/User/Preferences.sublime-settings"
  if [[ -f  "${HOME}/Library/Application Support/Code/User/settings.json" ]]; then rm "${HOME}/Library/Application Support/Code/User/settings.json" ; fi ; ln -s "${HOME}/.dotfiles/editors/vscode_settings.json" "${HOME}/Library/Application Support/Code/User/settings.json"
fi


#TODO: make ansible call this script.
# basic 'install' is here,
# advanced config is there.
